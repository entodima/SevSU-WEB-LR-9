<?php
use Illuminate\Support\Facades\App;
use App\Post;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('index');


Route::get('/posts', 'PostController@index')->name('posts-all');
Route::get('/posts/{task}', 'PostController@show')->name('post');

Auth::routes();
Route::get('/admin', 'AdminController@index')->name('admin');
Route::get('/admin/edit', 'AdminController@edit')->name('admin-edit');
Route::post('/admin/edit/add', 'AdminController@editAdd')->name('admin-edit-add');
Route::post('/admin/edit/add/many', 'AdminController@editAddMany')->name('admin-edit-add-many');
Route::post('/admin/edit/delete', 'AdminController@editDelete')->name('admin-edit-delete');
Route::post('/admin/edit/change', 'AdminController@editChange')->name('admin-edit-change');