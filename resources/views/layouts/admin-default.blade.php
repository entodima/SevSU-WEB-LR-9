<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8" />
    <title>Админ - @yield('title')</title>
    <link rel="stylesheet" type="text/css" href="/css/main.css" />
    <script src="/js/ajax.js"></script>
    <script src="/js/plugins/jquery-3.3.1.js"></script>
</head>

<body class="index">
<form id="logout-form" action="{{route('logout')}}" method="post">
    @csrf
</form>

<header>
    <div class="panel" id="panel">
        <a href="{{ route('index') }}"><img src="/img/logo.png" alt="logo"></a>
        <a href="{{ route('admin-edit') }}"><p>Редактировать блог</p></a>
        <a onclick="document.getElementById('logout-form').submit()"><p>Выход</p></a>
    </div>
    <script src="/js/panel.js"></script>
</header>

<div class="main">
    @yield('content')
</div>

</body>
</html>
