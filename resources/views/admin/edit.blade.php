@extends('layouts.admin-default')

@section('title')
    Редактор блога
@endsection

@section('content')
    <script src="/js/admin_edit.js"></script>
    <div>
        <h1>Добавить запись</h1>
        <form method="post" action="{{ route('admin-edit-add') }}" enctype="multipart/form-data">
            @csrf
            <input name="title" placeholder="Заголовок">
            <textarea name="text" placeholder="текст"></textarea>
            <input name="image" type="file">
            <input type="submit" value="Отправить">
        </form>
    </div>


    <div  style="margin: 50px 0">
        <h1>Загрузить файл записей</h1>
        <form method="post" action="{{ route('admin-edit-add-many') }}" enctype="multipart/form-data">
            @csrf
            <input name="file" type="file">
            <input type="submit" value="Отправить">
        </form>
    </div>

    @foreach($posts as $post)
        <div style="margin-bottom: 50px">
            <form action="{{ route('admin-edit-change') }}" method="post" class="admin-edit-form">
                @csrf
                <input type="hidden" value="{{$post->id}}" name="id">
                <input class="title" value="{{ $post->title }}" name="title">
                <p><i>{{ $post->created_at }}</i></p>
                <textarea name="text"  style="width: 100%; background: none; min-height: 100px">{{ $post->text }}</textarea>
                <a href="/img/{{$post->image}}" target="_blank" rel="noopener noreferrer" onclick="//selectFile({{$post->id}})"><img style="max-width: 200px" src="/img/{{ $post->image }}"></a><br>
                <input type="file" name="file" style="display: none" id="fileSelect{{$post->id}}">
                <input class="change-submit" type="submit" value="Изменить">
            </form>


            <form class="admin-delete-form" action="{{ route('admin-edit-delete') }}" method="post" style="margin-right: 5px">
                @csrf
                <input type="hidden" value="{{ $post->id }}" name="id">
                <input class="delete-submit" type="submit" value="Удалить" >
            </form>
        </div>
    @endforeach
    {{ $posts->links() }}
@endsection
