<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Пост</title>
</head>
<body>
    <h1>{{ $post->title }}</h1>
    <p>
        <i>{{ $post->created_at }}</i><br>
        {{ $post->text }}
    </p>
</body>
</html>