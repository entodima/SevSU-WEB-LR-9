@extends('layouts.default')

@section('title')
    Блог - все записи
@endsection

@section('content')
    @foreach($posts as $post)
        <div style="margin-bottom: 50px">
            <h2>{{ $post->title }}</h2>
            <p><i>{{ $post->created_at }}</i></p>
            <p>{{ $post->text }}</p>
            <a target="_blank" rel="noopener noreferrer" href="/img/{{ $post->image }}"><img style="max-width: 200px" src="/img/{{ $post->image }}"></a>
        </div>
    @endforeach
    {{ $posts->links() }}

@endsection