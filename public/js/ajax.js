function getLoginInfo(value) {
    var login_hint = document.getElementById('login-hint');
    if (value == ''){
        login_hint.innerHTML=value;
    } else {
        xmlhttp = new XMLHttpRequest();

        xmlhttp.onreadystatechange = function () {
            if (this.readyState==4 && this.status==200){
                login_hint.innerHTML = this.responseText;
            }
        };

        xmlhttp.open('GET', '/check_register.php?q='+value, true);
        xmlhttp.send();
    }
}

function getCommens(id) {
    var div = document.getElementById('comments'+id);
    xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (this.readyState==4 && this.status==200){
            div.innerHTML = this.responseText;
        }
    };

    xmlhttp.open('GET', '/get_comments.php?id='+id, true);
    xmlhttp.send();
}

function sendComment(comment, user_id, blog_id) {
    var div = document.getElementById('comments'+blog_id);
    xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (this.readyState==4 && this.status==200){
            getCommens(blog_id);
        }
    };

    xmlhttp.open('GET', '/send_comment.php?blogId='+blog_id+'&userId='+user_id+'&comment='+comment, true);
    xmlhttp.send();
}


//админ
function showChangeDiv(id) {
    $('#changeButton' + id).hide();
    $('#changeDiv' + id).show();
}
function hideChangeDiv(id) {
    $('#changeButton' + id).show();
    $('#changeDiv' + id).hide();
}
function saveBlog(id, form) {
    var someObj = {
        id:id,
        title:form.title.value,
        text:form.text.value,
    };
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/edit_blog.php');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send('param=' + JSON.stringify(someObj));
    xhr.onreadystatechange = function()
    {
        if (this.readyState == 4)
        {
            if (this.status == 200)
            {
                if (form.title.value != ''){
                    document.getElementById('blog-title-'+id).innerHTML = form.title.value;
                }
                if (form.text.value != ''){
                    document.getElementById('blog-text-'+id).innerHTML = form.text.value;
                }
                hideChangeDiv(id);
                form.title.value = null;
                form.text.value = null;
            } else {
                console.log('ajax error');
            }
        }
    };
}