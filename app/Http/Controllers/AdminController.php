<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return redirect()->route('admin-edit');
    }


    public function edit()
    {
        $posts = Post::orderByDesc('created_at')->paginate(2);
        return view('admin.edit', compact('posts'));
    }


    public function editAdd(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'text' => 'required',
        ]);
        if (!$validator->fails()) {
            $post = new Post();
            $post->title = $request->title;
            $post->text = $request->text;
            $post->created_at = Carbon::now();
            if ($request->image != null) {
                $post->image = Storage::disk('public')->put('blog', $request->image);
            }
            $post->save();
        }
        return redirect()->route('admin-edit');
    }


    public function editAddMany(Request $request)
    {
        if (($handle = fopen($request->file, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                for ($c = 0; $c < $num; $c++) {
                    echo $data[$c] . "<br />\n";

                    $temp = explode(';', $data[$c]);

                    $post = new Post();
                    $post->title = $temp[0];
                    $post->text = $temp[1];
                    $post->created_at = $temp[3];
                    $post->save();

                }
            }
            fclose($handle);

            return redirect()->route('admin-edit');
        }
    }

    public function editDelete(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if (!$validator->fails()) {
            $post = Post::find($request->id);
            Storage::disk('public')->delete('blog', $post->image);
            $post->delete();
        }

        return redirect()->route('admin-edit');
    }

    public function editChange(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if (!$validator->fails()) {
            $post = Post::find($request->id);

            $validator = Validator::make($request->all(), [
                'title' => 'required',
            ]);
            if (!$validator->fails()) {
                $post->title = $request->title;
            }

            $validator = Validator::make($request->all(), [
                'text' => 'required',
            ]);
            if (!$validator->fails()) {
                $post->text = $request->text;
            }

            $validator = Validator::make($request->all(), [
                'image' => 'required',
            ]);
            if (!$validator->fails()) {
                $post->image = Storage::disk('public')->put('blog', $request->image);
            }
            $post->save();
        }

        return redirect()->route('admin-edit');
    }
}